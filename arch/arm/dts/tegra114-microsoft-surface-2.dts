// SPDX-License-Identifier: GPL-2.0
/dts-v1/;

#include <dt-bindings/input/input.h>
#include "tegra114.dtsi"

/ {
	model = "Microsoft Surface 2";
	compatible = "microsoft,surface-2", "nvidia,tegra114";

	chosen {
		stdout-path = &uarta;
	};

	aliases {
		i2c0 = &pwr_i2c;

		mmc0 = &sdmmc4; /* eMMC */
		mmc1 = &sdmmc3; /* uSD slot */

		usb0 = &usb1;
	};

	memory {
		device_type = "memory";
		reg = <0x80000000 0x80000000>;
	};

	host1x@50000000 {
		dc@54200000 {
			clocks = <&tegra_car TEGRA114_CLK_DISP1>,
				 <&tegra_car TEGRA114_CLK_PLL_D_OUT0>;

			rgb {
				status = "okay";

				nvidia,panel = <&dsia>;
			};
		};

		dsia: dsi@54300000 {
			status = "okay";

			avdd-dsi-csi-supply = <&avdd_dsi_csi>;

			panel@0 {
				compatible = "samsung,ltl106hl02-001";
				reg = <0>;

				vdd-supply = <&tps65090_fet4>;

				backlight = <&backlight>;
			};
		};
	};

	pinmux@70000868 {
		pinctrl-names = "default";
		pinctrl-0 = <&state_default>;

		state_default: pinmux {
			/* ULPI pinmux */
			ulpi-data0 {
				nvidia,pins = "ulpi_data0_po1",
						"ulpi_data3_po4",
						"ulpi_data4_po5";
				nvidia,function = "ulpi";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			ulpi-data1 {
				nvidia,pins = "ulpi_data1_po2";
				nvidia,function = "ulpi";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			ulpi-data2 {
				nvidia,pins = "ulpi_data2_po3";
				nvidia,function = "ulpi";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			ulpi-data7 {
				nvidia,pins = "ulpi_data7_po0",
						"ulpi_data5_po6",
						"ulpi_data6_po7";
				nvidia,function = "ulpi";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};

			/* I2S pinmux */
			dap1-din {
				nvidia,pins = "dap1_fs_pn0",
						"dap1_din_pn1",
						"dap1_sclk_pn3";
				nvidia,function = "i2s0";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			dap1-dout {
				nvidia,pins = "dap1_dout_pn2";
				nvidia,function = "i2s0";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			dap2-i2s1 {
				nvidia,pins = "dap2_fs_pa2",
						"dap2_sclk_pa3",
						"dap2_din_pa4",
						"dap2_dout_pa5";
				nvidia,function = "i2s1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			dap3-i2s2 {
				nvidia,pins = "dap3_fs_pp0",
						"dap3_din_pp1",
						"dap3_dout_pp2",
						"dap3_sclk_pp3";
				nvidia,function = "i2s2";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			dap4-din {
				nvidia,pins = "dap4_fs_pp4",
						"dap4_din_pp5",
						"dap4_sclk_pp7";
				nvidia,function = "i2s3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			dap4-dout {
				nvidia,pins = "dap4_dout_pp6";
				nvidia,function = "i2s3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* SDMMC1 pinmux */
			sdmmc1-wp-clk {
				nvidia,pins = "sdmmc1_wp_n_pv3",
						"sdmmc1_clk_pz0";
				nvidia,function = "sdmmc1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			sdmmc1-cmd {
				nvidia,pins = "sdmmc1_cmd_pz1",
						"sdmmc1_dat3_py4",
						"sdmmc1_dat2_py5",
						"sdmmc1_dat1_py6",
						"sdmmc1_dat0_py7";
				nvidia,function = "sdmmc1";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};

			/* SDMMC3 pinmux */
			sdmmc3-clk {
				nvidia,pins = "sdmmc3_clk_pa6";
				nvidia,function = "sdmmc3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			sdmmc3-cmd {
				nvidia,pins = "sdmmc3_cmd_pa7",
						"sdmmc3_dat3_pb4",
						"sdmmc3_dat2_pb5",
						"sdmmc3_dat1_pb6",
						"sdmmc3_dat0_pb7",
						"sdmmc3_cd_n_pv2",
						"sdmmc3_clk_lb_in_pee5";
				nvidia,function = "sdmmc3";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			sdmmc3-clk-lb-out {
				nvidia,pins = "sdmmc3_clk_lb_out_pee4";
				nvidia,function = "sdmmc3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* SDMMC4 pinmux */
			sdmmc4-clk {
				nvidia,pins = "sdmmc4_clk_pcc4";
				nvidia,function = "sdmmc4";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			sdmmc4-cmd {
				nvidia,pins = "sdmmc4_cmd_pt7",
						"sdmmc4_dat0_paa0",
						"sdmmc4_dat1_paa1",
						"sdmmc4_dat2_paa2",
						"sdmmc4_dat3_paa3",
						"sdmmc4_dat4_paa4",
						"sdmmc4_dat5_paa5",
						"sdmmc4_dat6_paa6",
						"sdmmc4_dat7_paa7";
				nvidia,function = "sdmmc4";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};

			/* HDMI pinmux */
			hdmi-int {
				nvidia,pins = "hdmi_int_pn7";
				nvidia,function = "rsvd1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
				nvidia,rcv-sel = <TEGRA_PIN_DISABLE>;
			};
			hdmi-cec {
				nvidia,pins = "hdmi_cec_pee3";
				nvidia,function = "rsvd3";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};

			/* I2C pinmux */
			gen1-i2c {
				nvidia,pins = "gen1_i2c_scl_pc4",
						"gen1_i2c_sda_pc5";
				nvidia,function = "i2c1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};
			gen2-i2c {
				nvidia,pins = "gen2_i2c_scl_pt5",
						"gen2_i2c_sda_pt6";
				nvidia,function = "i2c2";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};
			cam-i2c {
				nvidia,pins = "cam_i2c_scl_pbb1",
						"cam_i2c_sda_pbb2";
				nvidia,function = "i2c3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};
			ddc-scl-pv4 {
				nvidia,pins = "ddc_scl_pv4",
						"ddc_sda_pv5";
				nvidia,function = "i2c4";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
				nvidia,rcv-sel = <TEGRA_PIN_DISABLE>;
			};
			pwr-i2c {
				nvidia,pins = "pwr_i2c_scl_pz6",
						"pwr_i2c_sda_pz7";
				nvidia,function = "i2cpwr";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};

			/* UARTA pinmux */
			uarta-out {
				nvidia,pins = "pu0", "pu3";
				nvidia,function = "uarta";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			uarta-in {
				nvidia,pins = "pu1", "pu2";
				nvidia,function = "uarta";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};

			/* UARTB pinmux */
			uart2-txd-pc2 {
				nvidia,pins = "uart2_txd_pc2";
				nvidia,function = "irda";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			uart2-rxd-pc3 {
				nvidia,pins = "uart2_rxd_pc3";
				nvidia,function = "irda";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			uart2-cts-n-pj5 {
				nvidia,pins = "uart2_cts_n_pj5",
						"uart2_rts_n_pj6";
				nvidia,function = "rsvd3";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* UARTC pinmux */
			uart3-cts-rxd {
				nvidia,pins = "uart3_cts_n_pa1",
						"uart3_rxd_pw7";
				nvidia,function = "uartc";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			uart3-rts-txd {
				nvidia,pins = "uart3_rts_n_pc0",
						"uart3_txd_pw6";
				nvidia,function = "uartc";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* UARTD pinmux */
			uartd-out {
				nvidia,pins = "ulpi_clk_py0",
						"ulpi_stp_py3";
				nvidia,function = "uartd";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			uartd-in {
				nvidia,pins = "ulpi_dir_py1",
						"ulpi_nxt_py2";
				nvidia,function = "uartd";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};

			/* GMI section */
			gmi-a17 {
				nvidia,pins = "gmi_a17_pb0",
						"gmi_a18_pb1",
						"gmi_iordy_pi5",
						"kb_col1_pq1",
						"kb_row8_ps0",
						"pbb6";
				nvidia,function = "rsvd2";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-wp-n {
				nvidia,pins = "gmi_wp_n_pc7",
						"gmi_cs0_n_pj0",
						"gpio_x7_aud_px7";
				nvidia,function = "rsvd1";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-ad0 {
				nvidia,pins = "gmi_ad0_pg0",
						"gmi_ad1_pg1",
						"gmi_ad2_pg2",
						"gmi_ad3_pg3",
						"gmi_ad4_pg4",
						"gmi_ad5_pg5",
						"gmi_ad6_pg6",
						"gmi_ad7_pg7",
						"gmi_oe_n_pi1",
						"pv1";
				nvidia,function = "rsvd1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-ad8 {
				nvidia,pins = "gmi_ad8_ph0";
				nvidia,function = "pwm0";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gmi-ad9 {
				nvidia,pins = "gmi_ad9_ph1",
						"gmi_ad10_ph2",
						"gmi_ad11_ph3",
						"gmi_ad15_ph7",
						"gmi_cs4_n_pk2";
				nvidia,function = "gmi";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gmi-ad12 {
				nvidia,pins = "gmi_ad12_ph4",
						"gmi_ad13_ph5",
						"gpio_x1_aud_px1",
						"pcc1",
						"clk3_req_pee1",
						"clk1_req_pee2";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gmi-ad14 {
				nvidia,pins = "gmi_ad14_ph6",
						"gmi_a16_pj7",
						"gmi_a19_pk7";
				nvidia,function = "gmi";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-wr-n {
				nvidia,pins = "gmi_wr_n_pi0";
				nvidia,function = "spi4";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-cs6-n {
				nvidia,pins = "gmi_cs6_n_pi3",
						"gmi_cs7_n_pi6";
				nvidia,function = "nand";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-rst-n {
				nvidia,pins = "gmi_rst_n_pi4",
						"spdif_out_pk5",
						"spdif_in_pk6",
						"clk2_out_pw5",
						"dvfs_pwm_px0",
						"dvfs_clk_px2",
						"pbb7",
						"pcc2",
						"clk2_req_pcc5";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gmi-wait {
				nvidia,pins = "gmi_wait_pi7";
				nvidia,function = "nand";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gmi-cs1-n {
				nvidia,pins = "gmi_cs1_n_pj2";
				nvidia,function = "soc";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-dqs-p {
				nvidia,pins = "gmi_dqs_p_pj3";
				nvidia,function = "sdmmc2";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gmi-adv-n {
				nvidia,pins = "gmi_adv_n_pk0";
				nvidia,function = "rsvd1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gmi-clk {
				nvidia,pins = "gmi_clk_pk1",
						"gmi_cs2_n_pk3",
						"gmi_cs3_n_pk4";
				nvidia,function = "gmi";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			jtag-rtck {
				nvidia,pins = "jtag_rtck";
				nvidia,function = "rtck";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* KBC pinmux */
			kb-col0 {
				nvidia,pins = "kb_col0_pq0",
						"kb_col3_pq3",
						"kb_col4_pq4",
						"kb_row4_pr4";
				nvidia,function = "kbc";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			kb-col2 {
				nvidia,pins = "kb_col2_pq2",
						"kb_col6_pq6",
						"kb_col7_pq7",
						"kb_row0_pr0",
						"kb_row2_pr2",
						"pv0",
						"sys_clk_req_pz5";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			kb-col5 {
				nvidia,pins = "kb_col5_pq5",
						"kb_row5_pr5";
				nvidia,function = "kbc";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			kb-row1 {
				nvidia,pins = "kb_row1_pr1";
				nvidia,function = "kbc";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			kb-row3 {
				nvidia,pins = "kb_row3_pr3",
						"kb_row9_ps1";
				nvidia,function = "rsvd3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			kb-row6 {
				nvidia,pins = "kb_row6_pr6";
				nvidia,function = "kbc";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			kb-row7 {
				nvidia,pins = "kb_row7_pr7",
						"pbb3",
						"pbb4",
						"pbb5";
				nvidia,function = "rsvd2";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			kb-row10 {
				nvidia,pins = "kb_row10_ps2";
				nvidia,function = "rsvd3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};

			/* CORE pinmux */
			clk-32k-out {
				nvidia,pins = "clk_32k_out_pa0";
				nvidia,function = "blink";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			clk-32k-in {
				nvidia,pins = "clk_32k_in";
				nvidia,function = "clk";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			core-pwr-req {
				nvidia,pins = "core_pwr_req";
				nvidia,function = "pwron";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			cpu-pwr-req {
				nvidia,pins = "cpu_pwr_req";
				nvidia,function = "cpu";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			pwr-int-n {
				nvidia,pins = "pwr_int_n";
				nvidia,function = "pmi";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			owr {
				nvidia,pins = "owr";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
				nvidia,rcv-sel = <TEGRA_PIN_DISABLE>;
			};
			reset-out-n {
				nvidia,pins = "reset_out_n";
				nvidia,function = "reset_out_n";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* AUD pinmux */
			gpio-w2-aud {
				nvidia,pins = "gpio_w2_aud_pw2";
				nvidia,function = "rsvd2";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gpio-w3-aud {
				nvidia,pins = "gpio_w3_aud_pw3";
				nvidia,function = "spi6";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gpio-x3-aud {
				nvidia,pins = "gpio_x3_aud_px3";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_UP>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gpio-x4-aud {
				nvidia,pins = "gpio_x4_aud_px4";
				nvidia,function = "rsvd1";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			gpio-x5-aud {
				nvidia,pins = "gpio_x5_aud_px5";
				nvidia,function = "rsvd1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			gpio-x6-aud {
				nvidia,pins = "gpio_x6_aud_px6";
				nvidia,function = "spi6";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			pu4 {
				nvidia,pins = "pu4";
				nvidia,function = "pwm1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};
			pu5 {
				nvidia,pins = "pu5";
				nvidia,function = "pwm2";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			pu6 {
				nvidia,pins = "pu6";
				nvidia,function = "pwm3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			pbb0 {
				nvidia,pins = "pbb0",
						"cam_mclk_pcc0";
				nvidia,function = "vi_alt1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			clk1-out {
				nvidia,pins = "clk1_out_pw4";
				nvidia,function = "extperiph1";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_ENABLE>;
			};
			clk3-out {
				nvidia,pins = "clk3_out_pee0";
				nvidia,function = "extperiph3";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
			};

			/* USB pinmux */
			usb-vbus-en0 {
				nvidia,pins = "usb_vbus_en0_pn4";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_NONE>;
				nvidia,tristate = <TEGRA_PIN_DISABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};
			usb-vbus-en1 {
				nvidia,pins = "usb_vbus_en1_pn5";
				nvidia,function = "rsvd4";
				nvidia,pull = <TEGRA_PIN_PULL_DOWN>;
				nvidia,tristate = <TEGRA_PIN_ENABLE>;
				nvidia,enable-input = <TEGRA_PIN_DISABLE>;
				nvidia,open-drain = <TEGRA_PIN_ENABLE>;
			};
		};
	};

	uarta: serial@70006000 {
		status = "okay";
	};

	pwm: pwm@7000a000 {
		status = "okay";
	};

	pwr_i2c: i2c@7000d000 {
		status = "okay";
		clock-frequency = <400000>;

		/* Texas Instruments TPS65090 PMIC */
		tps65090@48 {
			compatible = "ti,tps65090";
			reg = <0x48>;

			regulators {
				tps65090_fet1: fet1 {
					regulator-name = "vcd_led";
					regulator-boot-on;
				};

				tps65090_fet4: fet4 {
					regulator-name = "vdd_lcd";
					regulator-boot-on;
				};

				tps65090_fet6: fet6 {
					regulator-name = "vdd_usd";
					regulator-boot-on;
				};
			};
		};

		/* Texas Instruments TPS65913 PMIC */
		pmic: tps65913@58 {
			compatible = "ti,tps65913";
			reg = <0x58>;

			interrupts = <GIC_SPI 86 IRQ_TYPE_LEVEL_HIGH>;
			#interrupt-cells = <2>;
			interrupt-controller;

			ti,system-power-controller;

			pmic {
				compatible = "ti,tps65913-pmic";

				regulators {
					vdd_1v8_vio: smps8 {
						regulator-name = "vdd_1v8";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
						regulator-always-on;
						regulator-boot-on;
						regulator-ramp-delay = <5000>;
					};

					avdd_dsi_csi: ldo3 {
						regulator-name = "avdd_dsi_csi";
						regulator-min-microvolt = <1200000>;
						regulator-max-microvolt = <1200000>;
						regulator-boot-on;
					};

					vddio_usd: ldo9 {
						regulator-name = "vddio_usd";
						regulator-min-microvolt = <2900000>;
						regulator-max-microvolt = <2900000>;
						regulator-always-on;
						regulator-boot-on;
					};
				};
			};
		};
	};

	sdmmc3: sdhci@78000400 {
		status = "okay";
		bus-width = <4>;

		cd-gpios = <&gpio TEGRA_GPIO(V, 2) GPIO_ACTIVE_LOW>;

		nvidia,default-tap = <0x3>;
		nvidia,default-trim = <0x3>;

		vmmc-supply = <&tps65090_fet6>;
		vqmmc-supply = <&vddio_usd>;
	};

	sdmmc4: sdhci@78000600 {
		status = "okay";
		bus-width = <8>;
		non-removable;
	};

	usb1: usb@7d000000 {
		status = "okay";
		dr_mode = "otg";
		nvidia,vbus-gpio = <&gpio TEGRA_GPIO(K, 1) GPIO_ACTIVE_HIGH>;
	};

	usb-phy@7d000000 {
		status = "okay";

		nvidia,xcvr-setup = <7>;
		nvidia,xcvr-lsfslew = <2>;
		nvidia,xcvr-lsrslew = <2>;
	};

	backlight: backlight {
		compatible = "pwm-backlight";

		enable-gpios = <&gpio TEGRA_GPIO(CC, 2) GPIO_ACTIVE_HIGH>;
		power-supply = <&tps65090_fet1>;
		pwms = <&pwm 0 1000000>;

		brightness-levels = <1 35 70 105 140 175 210 255>;
		default-brightness-level = <5>;
	};

	/* PMIC has a built-in 32KHz oscillator which is used by PMC */
	clk32k_in: clock-32k {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <32768>;
		clock-output-names = "pmic-oscillator";
	};

	extcon-keys {
		compatible = "gpio-keys";

		switch-hall-sensor {
			label = "Hall Sensor";
			gpios = <&gpio TEGRA_GPIO(R, 4) GPIO_ACTIVE_HIGH>;
			linux,code = <SW_LID>;
		};
	};

	gpio-keys {
		compatible = "gpio-keys";

		key-power {
			label = "Power Button";
			gpios = <&gpio TEGRA_GPIO(V, 0) GPIO_ACTIVE_HIGH>;
			linux,code = <KEY_ENTER>;
		};

		key-volume-down {
			label = "Volume Down";
			gpios = <&gpio TEGRA_GPIO(R, 1) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_DOWN>;
		};

		key-volume-up {
			label = "Volume Up";
			gpios = <&gpio TEGRA_GPIO(R, 2) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_UP>;
		};

		key-windows {
			label = "Windows Button";
			gpios = <&gpio TEGRA_GPIO(I, 5) GPIO_ACTIVE_HIGH>;
			linux,code = <KEY_ENTER>;
		};
	};
};
