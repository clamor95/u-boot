// SPDX-License-Identifier: GPL-2.0
/dts-v1/;

#include <dt-bindings/input/input.h>

#include "tegra20.dtsi"

/ {
	model = "Motorola Atrix 4G (MB860)";
	compatible = "motorola,olympus", "nvidia,tegra20";

	chosen {
		stdout-path = &uartb;
	};

	aliases {
		i2c0 = &pwr_i2c;

		mmc0 = &sdmmc4;	/* eMMC */
		mmc1 = &sdmmc3; /* uSD slot */

		rtc1 = "/rtc@7000e000";

		usb0 = &micro_usb;
	};

	memory {
		device_type = "memory";
		reg = <0x00000000 0x40000000>;
	};

	uartb: serial@70006040 {
		status = "okay";
	};

	pwr_i2c: i2c@7000d000 {
		status = "okay";
		clock-frequency = <400000>;
	};

	micro_usb: usb@c5000000 {
		status = "okay";
		dr_mode = "otg";
	};

	usb-phy@c5000000 {
		status = "okay";
	};

	sdmmc3: sdhci@c8000400 {
		status = "okay";
		bus-width = <4>;

		cd-gpios = <&gpio TEGRA_GPIO(I, 5) GPIO_ACTIVE_LOW>;
	};

	sdmmc4: sdhci@c8000600 {
		status = "okay";
		bus-width = <8>;
		non-removable;
	};

	/* 32KHz oscillator which is used by PMC */
	clk32k_in: clock-32k-in {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <32768>;
		clock-output-names = "ref-oscillator";
	};
};
